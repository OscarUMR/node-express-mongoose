var Comments = require('../models/comments');

exports.readComments = function (req, res) {
    Comments.find({})
    .then(function(comments){
      res.json(comments);
    })
    .catch(function(error){
      res.json(eror);
    });
};

exports.readOneComments = function(req, res) {
  console.log('Updating comments with ID ' + req.params.id);
  comments.findById(req.params.id)
  .then(function(comments){
    res.json(comments);
  })
  .catch(function(error){
    console.error('Error updating comments with ID ' + req.params.id, error);
    res.json(error);
  })
};

exports.createComments = function (req, res) {
  var newComments = new Comments(req.body);
  newComments.save()
  .then(function(comments) {
    res.json(comments);
  })
  .catch(function(error) {
    res.json(error)
  });
};

exports.updateOneComments = function(req, res){
  var id = req.params.id;
  var options = {
    new: true
  };
  Comments.findByIdAndUpdate(id, req.body, options)
    .then(function(comments){
      res.json(comments);
    })
    .catch(function(error) {
      console.error('Error updating comments with ID ' + req.params.id, error);
      res.json(error);
    });
};

exports.deleteOneComments = function(req, res){
  var id = req.params.id
  Comments.findByIdAndRemove(id)
    .then(function(comments){
      res.json({});
    })
    .catch(function(error) {
      console.error('Error deleting comments with ID ' + req.params.id, error);
      res.json(error);
    });
};
