var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SchemaOptions = {
  collection: 'comments',
  versionKey: 'version',
  timestamps: true
};

var SchemaProperties = {
  article: {
    type: Schema.Types.ObjectId,
    ref: 'Article'
  },

  content:{
    require: true,
    type: String,
    maxlength: 500,
  },

  autor:{
    require: true,
    type: String
  },

  postedAt: {
    type: Date,
    default: Date.now
  }
};

var commentsSchema = new Schema(SchemaProperties, SchemaOptions);

module.exports = mongoose.model('Comments', commentsSchema)
