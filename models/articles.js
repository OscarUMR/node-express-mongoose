var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SchemaOptions = {
  collection: 'articles',
  versionKey: 'version',
  timestamps: true
};

var SchemaProperties = {
  title: {
    require: true,
    type: String
  },

  content:{
    require: true,
    type: String,
    maxlength: 500,
  },

  autor:{
    require: true,
    type: String
  },

  comments: {
    type: Array,
    default: []
  },

  link: {
    require: true,
    type: String
  }
};

var articlesSchema = new Schema(SchemaProperties, SchemaOptions);

module.exports = mongoose.model('Article', articlesSchema)
