var router = require('express').Router();
var articlesController = require('./controllers/articles');
var commentsController = require('./controllers/comments');


//articles//

router.get('/articles', articlesController.readArticles);
router.post('/articles', articlesController.createArticle);
router.get('/articles/:id', articlesController.readOneArticle);

router.put('/articles/:id', articlesController.updateOneArticle);

router.delete('/articles/:id', articlesController.deleteOneArticle);

module.exports = router;
// router.get('/articles', function(req, res) {
//   console.log('Reading articles');
//   res.send('This is not implemented now');
// });

// router.post('/articles', function(req, res) {
//   console.log('Creating article')
//   console.log(req.body);
//   res.send('This is not implemented now');
// });

// comment //

router.get('/comments', commentsController.readComments);
router.post('/comments', commentsController.createComments);



router.get('/comments/:id', commentsController.readOneComments);

router.put('/comments/:id', commentsController.updateOneComments);

router.delete('/comments/:id', commentsController.deleteOneComments);
